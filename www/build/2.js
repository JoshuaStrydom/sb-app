webpackJsonp([2],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedPageModule", function() { return FeedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feed__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FeedPageModule = (function () {
    function FeedPageModule() {
    }
    FeedPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__feed__["a" /* FeedPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__feed__["a" /* FeedPage */]),
            ],
        })
    ], FeedPageModule);
    return FeedPageModule;
}());

//# sourceMappingURL=feed.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FeedPage = (function () {
    function FeedPage(fire, navCtrl, navParams, restProvider, app) {
        this.fire = fire;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restProvider = restProvider;
        this.app = app;
        // this.email = fire.auth.currentUser.email;
        this.getUsers();
    }
    FeedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedPage');
    };
    FeedPage.prototype.getUsers = function () {
        var _this = this;
        this.restProvider.getUsers()
            .then(function (data) {
            _this.users = data;
            console.log(_this.users);
        });
    };
    FeedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-feed',template:/*ion-inline-start:"C:\Users\joshl\Documents\dev\apps\sb-app\src\pages\feed\feed.html"*/'<!--\n\n  Generated template for the FeedPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Feed</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content style="background:#f7f7f7;">\n\n  <!-- Refresh to get the new posts -->\n\n  <!--<ion-refresher pulling-text="Pull to refresh..." on-refresh="getNewData()">\n\n  </ion-refresher>-->\n\n\n\n  <div class="row filtered-feed-header-outer">\n\n    <!-- <div class="col filtered-feed-header">\n\n       <div multi-bg="[current_category.image]" interval="3000" helper-class="category-with-image">\n\n         <div class="filtered-feed-header-content">\n\n           <h2 class="header-title">{{email}}</h2>\n\n         </div>\n\n       </div>\n\n     </div>-->\n\n    <!--<div  class="col filtered-feed-header">\n\n      <div multi-bg="[current_trend.image]" interval="3000" helper-class="category-with-image">\n\n        <div class="filtered-feed-header-content">\n\n          <h2 class="header-title">{{email}}</h2>\n\n        </div>\n\n      </div>\n\n    </div>-->\n\n  </div>\n\n\n\n  <div class="card new-post" style="">\n\n    <div class="item item-avatar" style="">\n\n      <div class="card-avatar-image-outer">\n\n        <img class="profile-avatar" src="https://www.studentbrands.co.za/wp-content/uploads/2018/04/SIGNING-741x486.jpg" style="">\n\n        <!-- <pre-img ratio="_1_1" helper-class="avatar-image">\n\n           <img spinner-on-load>\n\n         </pre-img>-->\n\n      </div>\n\n      <a class="new-post-call-to-action" ng-click="newStatusPost()">\n\n        <h3 class="new-post-copy" style="">What\'s on your mind?</h3>\n\n      </a>\n\n    </div>\n\n    <div class="item tabs tabs-icon-left">\n\n      <a class="tab-item" ng-click="newStatusPost()">\n\n        <i class="icon ion-edit"></i>\n\n        Status\n\n      </a>\n\n      <a class="tab-item" ng-click="newImageStatusPost()">\n\n        <i class="icon ion-camera"></i>\n\n        Photo\n\n      </a>\n\n      <a class="tab-item" ng-click="checkinStatusPost()">\n\n        <i class="icon ion-location"></i>\n\n        Checkin\n\n      </a>\n\n    </div>\n\n  </div>\n\n\n\n  <div class="feed-cards-outer">\n\n    <!---->\n\n    <div class="card-1">\n\n      <div class="list card">\n\n        <div class="item item-image">\n\n          <a class="card-image-anchor">\n\n\n\n            <img class="post-image" src="https://www.studentbrands.co.za/wp-content/uploads/2018/04/SIGNING-741x486.jpg">\n\n\n\n          </a>\n\n        </div>\n\n        <div class="item item-body">\n\n          <a>\n\n            <h2 class="card-title">Fuck Boys, a crew not to be fucked with.</h2>\n\n          </a>\n\n        </div>\n\n        <div class="item item-avatar">\n\n          <div class="card-avatar-image-outer">\n\n            <img class="profile-avatar" src="https://www.studentbrands.co.za/wp-content/uploads/2018/04/SIGNING-741x486.jpg" style="">\n\n            <!-- <pre-img ratio="_1_1" helper-class="avatar-image">\n\n               <img spinner-on-load>\n\n             </pre-img>-->\n\n          </div>\n\n          <a class="new-post-call-to-action" ng-click="newStatusPost()">\n\n            <h3 class="" id="avatar-title-id" style="">Jackie Brown</h3>\n\n            <h3 class="" id="timestamp-id" style="">2 Hours Ago</h3>\n\n          </a>\n\n\n\n          <!--USE THESE WHEN CONNECTING BAKEND-->\n\n          <!-- <a>\n\n             <h2 class="avatar-title">Jackie Brown</h2>\n\n           </a>\n\n           <p class="avatar-description"><span am-time-ago="card.date"></span></p>\n\n           <a>\n\n             <span class="card-tag post-category">Catagory Name</span>\n\n           </a>-->\n\n        </div>\n\n        <div class="actions-brief">\n\n          <a class="subdued">100 Likes</a>\n\n          <a ng-controller="CommentsCtrl" ng-click="showComments(card)" class="subdued">20 Comments</a>\n\n          <a class="subdued">15 Shares</a>\n\n        </div>\n\n        <div class="item tabs tabs-icon-left">\n\n          <a class="tab-item" ng-class="{liked: card.liked}">\n\n            <i class="icon theme-icon icon-like"></i>\n\n            Like\n\n          </a>\n\n          <a class="tab-item" ng-controller="CommentsCtrl" ng-click="showComments(card)">\n\n            <i class="icon theme-icon icon-bubble"></i>\n\n            Comment\n\n          </a>\n\n          <a class="tab-item" social-share share="card">\n\n            <i class="icon theme-icon icon-share"></i>\n\n            Share\n\n          </a>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <!---->\n\n\n\n    <!-- CARD REPEATER -->\n\n    <!--<div ng-if="cards.length > 0" ng-repeat="card in cards" class="post-card-outer">\n\n       <section ng-include="\'views/app/partials/feed-post-card.html\'" class="post-card-outer"></section>\n\n      <!--<section ng-include="\'src/partials/feed-post-card.html\'"></section>-->\n\n  </div>\n\n\n\n  <!-- WHEN THERE ARE NO POSTS -->\n\n  <!--<div class="row row-center empty-feed-outer">\n\n    <div class="col">\n\n      <i class="empty-feed-icon icon ion-sad-outline"></i>\n\n      <h2 class="empty-feed-text">No posts here</h2>\n\n    </div>\n\n  </div>-->\n\n\n\n\n\n  <!-- Infinit scroll-->\n\n  <ion-infinite-scroll  on-infinite="loadMoreData()" distance="2%">\n\n  </ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\joshl\Documents\dev\apps\sb-app\src\pages\feed\feed.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]) === "function" && _e || Object])
    ], FeedPage);
    return FeedPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=feed.js.map

/***/ })

});
//# sourceMappingURL=2.js.map