import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import { HomePage } from '../pages/home/home';
import { LoggedinPage} from "../pages/loggedin/loggedin";
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { RestProvider } from '../providers/rest/rest';
import {WelcomePage} from "../pages/welcome/welcome";

  const firebaseAuth ={
      apiKey: "AIzaSyBnjyjSRsOsYcfs-kG94ZQE_ijfsDipqaw",
      authDomain: "sb-app-92b0c.firebaseapp.com",
      databaseURL: "https://sb-app-92b0c.firebaseio.com",
      projectId: "sb-app-92b0c",
      storageBucket: "sb-app-92b0c.appspot.com",
      messagingSenderId: "831241527298"
  };


@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    LoginPage,
    RegisterPage,
      HomePage,
    LoggedinPage

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    LoginPage,
    RegisterPage,
      HomePage,
    LoggedinPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider
  ]
})
export class AppModule {}
