import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the LoggedinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loggedin',
  templateUrl: 'loggedin.html',
})
export class LoggedinPage {

  email: string;
  users: any;
  constructor(private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider, public app: App) {

    this.email = fire.auth.currentUser.email;
    this.getUsers();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoggedinPage');
  }

  getUsers() {
    this.restProvider.getUsers()
      .then(data => {
        this.users = data;
        console.log(this.users);
      });
  }


}
